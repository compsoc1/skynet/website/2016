# 2009 - 2016

This is roughly what skynet.ie looked like from July 2009 to Oct 2016

This repo is from an archive found on the server in January 2023.  
As far as I can tell this archive was pieced together from the Wayback Machine.  

It is now packaged up and should be available under <https://2016.skynet.ie>