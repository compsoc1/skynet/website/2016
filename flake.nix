{
  description = "Skynet main site - 2003 to 2012";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages."${system}";
    in rec {

      # `nix build`
      defaultPackage = pkgs.stdenv.mkDerivation {
        name = "skynet-website_2003-2012";
        src = self;
        installPhase = "mkdir -p $out; cp -R src/* $out";
      };
    }
  );
}