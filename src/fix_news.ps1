Get-ChildItem -Path ./*.html%* | ForEach-Object {
  $NewName = ($_.Name -split "%")[0]
  Write-Host $_ $NewName
  Rename-Item -Path $_.FullName -NewName $NewName
}